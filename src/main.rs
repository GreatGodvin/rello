use std::io;
fn main() {
    println!("Hello World");
    let mut a = String::new();
    println!("Enter your input");
    io::stdin()
    .read_line(&mut a)
    .expect("Failed to read line");
    println!("Your input was {}", a);
}
